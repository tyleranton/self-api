const express = require('express');
const router = express.Router();
const meController = require('../controllers').me;
const projectController = require('../controllers').project;

/* GET home page. */
router.get('/', (req, res, next) => res.send('Ok'));

router.get('/api/me/:id', meController.getById);
router.post('/api/me', meController.add);

router.get('/api/project/:id', projectController.getById);
router.post('/api/project', projectController.add);
router.put('/api/project/:id', projectController.update);

module.exports = router;
