const me = require('./me');
const project = require('./project');

module.exports = {
  me,
  project,
};
