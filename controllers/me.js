const Me = require('../models').me;

module.exports = {
  getById(req, res) {
    return Me
      .findById(req.params.id)
      .then(me => {
        if (!me) {
          return res.status(404).send({
            message: 'Me Not Found'
          });
        }

        return res.status(200).send(me);
      })
      .catch(error => res.status(400).send(error));
  },

  add(req, res) {
    return Me
      .create({ ...req.body })
      .then(me => res.status(201).send(me))
      .catch(error => res.status(400).send(error));
  }
};
