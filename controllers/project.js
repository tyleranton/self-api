const Project = require('../models').Project;

module.exports = {
  getById(req, res) {
    findById(req.params.id)
    .then(project => {
      if (!project) {
        return res.status(404).send({
          message: 'Project Not Found'
        });
      }

      return res.status(200).send(project);
    })
    .catch(error => res.status(400).send(error));
  },

  add(req, res) {
    return Project
      .create({ ...req.body })
      .then(me => res.status(201).send(project))
      .catch(error => res.status(400).send(error));
  },

  update(req, res) { 
    return Project
      .findById(req.params.id)
      .then(me => {
        if (!project) {
          return res.status(404).send({
            message: 'Project Not Found'
          });
        }

        return Project
          .update({ ...req.body })
          .then(() => res.status(200).send(project))
          .catch(error => res.status(400).send(error));
      })
      .catch(error => res.status(400).send(error));
  }
};
