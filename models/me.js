'use strict';
module.exports = (sequelize, DataTypes) => {
  const me = sequelize.define('me', {
    first_name: DataTypes.STRING,
    middle_name: DataTypes.STRING,
    last_name: DataTypes.STRING,
    preferred_name: DataTypes.STRING,
  }, {});
  me.associate = function(models) {
    me.hasMany(models.Project, {
      foreignKey: 'me_id',
      as: 'projects'
    });
  };
  return me;
};
